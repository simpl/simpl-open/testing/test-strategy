package com.brooklyn.api.auth;

import com.brooklyn.api.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/** Utility class for handling JWT token creation, validation, and parsing. */
@Component
public class JwtUtil {

  private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);

  @Value("${jwt.secret}")
  private String secretKey;

  private long accessTokenValidity = 60L * 60 * 1000; // 1 hour validity for tokens
  private static final String TOKEN_HEADER = "Authorization";
  private static final String TOKEN_PREFIX = "Bearer ";
  private JwtParser jwtParser;

  /** Initializes the JwtParser with the secret key. */
  @PostConstruct
  public void init() {
    this.jwtParser = Jwts.parser().setSigningKey(secretKey);
  }

  /**
   * Creates a JWT token for the given user.
   *
   * @param user The user for whom the token is to be created.
   * @return The generated JWT token.
   */
  public String createToken(User user) {
    Claims claims = Jwts.claims().setSubject(user.getEmail());
    claims.put("firstName", user.getFirstName());
    claims.put("lastName", user.getLastName());
    Date tokenCreateTime = new Date();
    Date tokenValidity =
        new Date(tokenCreateTime.getTime() + TimeUnit.MINUTES.toMillis(accessTokenValidity));

    String token =
        Jwts.builder()
            .setClaims(claims)
            .setExpiration(tokenValidity)
            .signWith(SignatureAlgorithm.HS256, secretKey.getBytes())
            .compact();

    logger.debug("Generated Token: {}", token);
    logger.debug("Token Claims: {}", claims);

    return token;
  }

  /**
   * Parses the JWT token and extracts the claims.
   *
   * @param token The JWT token to be parsed.
   * @return The extracted claims.
   */
  private Claims parseJwtClaims(String token) {
    logger.debug("Parsing token: {}", token);
    return jwtParser.parseClaimsJws(token).getBody();
  }

  /**
   * Resolves and parses the JWT token from the request.
   *
   * @param req The HTTP request containing the JWT token.
   * @return The claims extracted from the token, or empty claims if no token is found.
   */
  public Claims resolveClaims(HttpServletRequest req) {
    try {
      String token = resolveToken(req);
      if (token != null) {
        logger.debug("Token found: {}", token);
        return parseJwtClaims(token);
      } else {
        logger.warn("No token found in request");
        return new DefaultClaims(Collections.emptyMap());
      }
    } catch (ExpiredJwtException ex) {
      String errorMessage =
          String.format(
              "Token expired for request to %s: %s", req.getRequestURI(), ex.getMessage());
      logger.warn(errorMessage);
      req.setAttribute("expired", ex.getMessage());
      throw new ExpiredJwtException(ex.getHeader(), ex.getClaims(), errorMessage, ex);
    } catch (JwtException ex) {
      String errorMessage =
          String.format(
              "Token validation failed for request to %s: %s",
              req.getRequestURI(), ex.getMessage());
      logger.error(errorMessage);
      req.setAttribute("invalid", ex.getMessage());
      throw new BadCredentialsException(errorMessage, ex);
    }
  }

  /**
   * Extracts the JWT token from the Authorization header of the request.
   *
   * @param request The HTTP request.
   * @return The JWT token, or null if the token is not present or invalid.
   */
  public String resolveToken(HttpServletRequest request) {
    String bearerToken = request.getHeader(TOKEN_HEADER);
    logger.debug("Authorization Header: {}", bearerToken);
    if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
      return bearerToken.substring(TOKEN_PREFIX.length());
    }
    logger.warn("Bearer token not found or invalid");
    return null;
  }

  /**
   * Validates the claims extracted from a JWT token.
   *
   * @param claims The claims to be validated.
   * @return True if the claims are valid, false otherwise.
   * @throws AuthenticationException if the claims are invalid or an unexpected error occurs.
   */
  public boolean validateClaims(Claims claims) throws AuthenticationException {
    try {
      boolean isValid = claims.getExpiration().after(new Date());
      logger.debug("Claims are valid: {}", isValid);
      return isValid;
    } catch (IllegalArgumentException e) {
      logger.error("Claims validation failed due to an invalid argument. Claims: {}", claims);
      throw new IllegalArgumentException("Failed to validate claims: " + e.getMessage(), e);
    } catch (Exception e) {
      logger.error("Unexpected error during claims validation. Claims: {}", claims);
      throw new BadCredentialsException(
          "An unexpected error occurred while validating claims: " + e.getMessage(), e);
    }
  }

  /**
   * Extracts the email (subject) from the claims.
   *
   * @param claims The claims from which the email is to be extracted.
   * @return The extracted email.
   */
  public String getEmail(Claims claims) {
    String email = claims.getSubject();
    logger.debug("Extracted email from claims: {}", email);
    return email;
  }

  /**
   * Extracts the roles from the claims.
   *
   * @param claims The claims from which the roles are to be extracted.
   * @return The list of roles, or an empty list if no roles are present.
   */
  @SuppressWarnings({"unused", "unchecked"})
  private List<String> getRoles(Claims claims) {
    List<String> roles = (List<String>) claims.get("roles");
    logger.debug("Extracted roles from claims: {}", roles);
    return roles;
  }
}
