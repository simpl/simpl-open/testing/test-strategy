package com.brooklyn.api.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the request data for user login.
 *
 * <p>This class is used to encapsulate the login credentials (email and password) provided by the
 * user when attempting to authenticate.
 *
 * <p>Example usage:
 *
 * <pre>{@code
 * LoginReq loginRequest = new LoginReq("user@example.com", "password123");
 * }</pre>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginReq {

  /** The email of the user attempting to log in. */
  private String email;

  /** The password of the user attempting to log in. */
  private String password;
}
