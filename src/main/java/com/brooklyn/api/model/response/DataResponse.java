package com.brooklyn.api.model.response;

/**
 * A generic response class that holds data of a specified type.
 *
 * @param <T> the type of data this response holds
 */
public class DataResponse<T> implements ApiResponse {

  private T data;

  /**
   * Constructs a new DataResponse object with the specified data.
   *
   * @param data the data to be encapsulated in the response
   */
  public DataResponse(T data) {
    this.data = data;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
