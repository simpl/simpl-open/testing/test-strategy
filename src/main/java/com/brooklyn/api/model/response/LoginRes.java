package com.brooklyn.api.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the response containing user login details.
 *
 * <p>This class is used to encapsulate the response data after a successful user login, which
 * includes the user's email and the JWT token generated for authentication.
 *
 * <p>Example usage:
 *
 * <pre>{@code
 * LoginRes loginResponse = new LoginRes("user@example.com", "jwt-token-value");
 * }</pre>
 *
 * @see ApiResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRes implements ApiResponse {

  /** The email of the user. */
  private String email;

  /** The JWT token issued to the user after successful authentication. */
  private String token;
}
