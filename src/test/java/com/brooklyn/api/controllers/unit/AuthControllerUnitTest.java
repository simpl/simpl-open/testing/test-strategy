package com.brooklyn.api.controllers.unit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.brooklyn.api.auth.JwtUtil;
import com.brooklyn.api.controllers.AuthController;
import com.brooklyn.api.model.User;
import com.brooklyn.api.model.request.LoginReq;
import com.brooklyn.api.model.request.RegistrationReq;
import com.brooklyn.api.model.response.ApiResponse;
import com.brooklyn.api.model.response.ErrorRes;
import com.brooklyn.api.model.response.LoginRes;
import com.brooklyn.api.model.response.UserListResponse;
import com.brooklyn.api.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Collections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

class AuthControllerUnitTest {

  @Mock private AuthenticationManager authenticationManager;

  @Mock private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Mock private UserService userService;

  @Mock private JwtUtil jwtUtil;

  @InjectMocks private AuthController authController;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void testLogin_Success() {
    // Arrange
    LoginReq loginReq = new LoginReq();
    loginReq.setEmail("test@example.com");
    loginReq.setPassword("password");

    Authentication authentication = mock(Authentication.class);
    when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
        .thenReturn(authentication);
    when(authentication.getName()).thenReturn("test@example.com");
    when(jwtUtil.createToken(any(User.class))).thenReturn("mockToken");

    // Act
    ResponseEntity<ApiResponse> response = authController.login(loginReq);

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertTrue(response.getBody() instanceof LoginRes);
    LoginRes loginRes = (LoginRes) response.getBody();
    assertEquals("test@example.com", loginRes.getEmail());
    assertEquals("mockToken", loginRes.getToken());
  }

  @Test
  void testLogin_BadCredentials() {
    // Arrange
    LoginReq loginReq = new LoginReq();
    loginReq.setEmail("wrong@example.com");
    loginReq.setPassword("wrongPassword");

    when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
        .thenThrow(new BadCredentialsException("Invalid credentials"));

    // Act
    ResponseEntity<ApiResponse> response = authController.login(loginReq);

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertTrue(response.getBody() instanceof ErrorRes);
    ErrorRes errorRes = (ErrorRes) response.getBody();
    assertEquals("Invalid username or password", errorRes.getMessage());
  }

  @Test
  void testRegister_Success() {
    // Arrange
    RegistrationReq registrationReq = new RegistrationReq();
    registrationReq.setEmail("newuser@example.com");
    registrationReq.setFirstName("John");
    registrationReq.setLastName("Doe");
    registrationReq.setPassword("newPassword");

    User mockUser = new User();
    mockUser.setEmail("newuser@example.com");
    when(bCryptPasswordEncoder.encode(anyString())).thenReturn("encodedPassword");
    when(userService.createUser(any(User.class))).thenReturn(mockUser);
    when(jwtUtil.createToken(any(User.class))).thenReturn("mockToken");

    // Act
    ResponseEntity<ApiResponse> response =
        authController.register(
            mock(HttpServletRequest.class), mock(HttpServletResponse.class), registrationReq);

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertTrue(response.getBody() instanceof LoginRes);
    LoginRes loginRes = (LoginRes) response.getBody();
    assertEquals("newuser@example.com", loginRes.getEmail());
    assertEquals("mockToken", loginRes.getToken());
  }

  @ParameterizedTest
  @ValueSource(longs = {1L, 2L, 3L})
  void testDeleteUser_Success(Long userId) {
    // Arrange
    when(userService.deleteUserById(userId)).thenReturn(true);

    // Act
    ResponseEntity<ApiResponse> response = authController.deleteUser(userId);

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @ParameterizedTest
  @ValueSource(longs = {1L, 2L, 3L})
  void testDeleteUser_Failure(Long userId) {
    // Arrange
    when(userService.deleteUserById(userId)).thenReturn(false);

    // Act
    ResponseEntity<ApiResponse> response = authController.deleteUser(userId);

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertTrue(response.getBody() instanceof ErrorRes);
    ErrorRes errorRes = (ErrorRes) response.getBody();
    assertEquals("User not found or could not be deleted", errorRes.getMessage());
  }

  @Test
  void testDeleteAllUsers_Success() {
    // Arrange
    doNothing().when(userService).deleteAllUsers();

    // Act
    ResponseEntity<Void> response = authController.deleteAllUsers();

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  void testGetAllUsers_Success() {
    // Arrange
    User mockUser = new User();
    mockUser.setEmail("user@example.com");
    when(userService.getAllUsers()).thenReturn(Collections.singletonList(mockUser));

    // Act
    ResponseEntity<ApiResponse> response = authController.getAllUsers();

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertTrue(response.getBody() instanceof UserListResponse);
    UserListResponse userListResponse = (UserListResponse) response.getBody();
    assertEquals(1, userListResponse.getUsers().size());
    assertEquals("user@example.com", userListResponse.getUsers().get(0).getEmail());
  }

  @Test
  void testGetAllUsers_Exception() {
    // Arrange
    when(userService.getAllUsers()).thenThrow(new RuntimeException("Unexpected error"));

    // Act
    ResponseEntity<ApiResponse> response = authController.getAllUsers();

    // Assert
    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertTrue(response.getBody() instanceof ErrorRes);
    ErrorRes errorRes = (ErrorRes) response.getBody();
    assertEquals("Unexpected error", errorRes.getMessage());
  }
}
