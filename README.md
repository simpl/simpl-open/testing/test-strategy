# Enabling Clean Code in Simpl-Open

<strong><span style="color: red;">W.I.P</span></strong> - aim of the sandbox is to provide examples and test code quality components to ensure Simpl delivers the baseline for clean code development. This will incl. tools like spotless, checkstyle, allure, sufire ... + how to integrate Google Coding Style Guide. And some more ... everything will be later fully documented in Confluence.

---

## Table of Contents

[1. Maven Wrapper](#1-maven-wrapper)  
 [2. Spotless - Maven Plugin](#2-spotless---maven-plugin)  
 [3. Checkstyle](#3-checkstyle)  
 [4. Allure reports](#4-allure-reports)  
 [5. Using REST assured](#5-using-rest-assured)  
 [6. Using Playwright](#6-using-playwright)  
 [7. Code Documentation and Java Doc](#7-code-documentation-and-java-doc)  
 [8. vscode set-up](#8-vscode-set-up)  
 [9. Intellij set-up](#9-intellij-set-up)  
 [10. Derived Guiding principles - Simpl Clean Code](#10-derived-guiding-principles---simpl-clean-code)  
 [11. Playing with the sandbox](#11-playing-with-the-sandbox)  
 [12. Test case storage for "int" and "unit"](#12-test-case-storage-for-int-and-unit)  
 [13. Test runner local pipeline](#13-test-runner-local-pipeline)

## 1. Maven Wrapper

The Maven Wrapper is a crucial tool for ensuring that all developers and CI/CD environments use the same version of Maven to build a project. By including the Maven Wrapper in each repo, we eliminate discrepancies that might arise from different Maven versions being used by different team members or build servers, which can lead to failing our central pipeline.

### Key Benefits of Using Maven Wrapper

- **Consistency Across Environments**: The Maven Wrapper guarantees that your project is always built with the same Maven version, regardless of the environment or developer machine. This is particularly useful in larger teams or when on-boarding new developers, as they won't need to manually install a specific Maven version.

- **Ease of Setup**: New developers or CI environments do not need to install Maven manually. They can simply use the provided wrapper scripts (`mvnw` for Unix-based systems and `mvnw.cmd` for Windows) to run Maven commands.

- **Version Control**: The Maven Wrapper configuration is committed to your project's version control system (VCS), ensuring that everyone uses the same Maven setup without needing to configure it individually.

- **Project Portability**: With the Maven Wrapper, your project becomes more portable. Anyone can clone the repository and immediately build the project using the provided wrapper scripts, without worrying about Maven installation or version issues.

### How to Add Maven Wrapper to Your Project

To add the Maven Wrapper to your existing Maven project, run the following command:

```bash
mvn -N wrapper:wrapper
```

This command will generate the necessary wrapper files in your project:

- `mvnw`: A shell script for Unix-based systems.
- `mvnw.cmd`: A batch script for Windows systems.
- `.mvn/wrapper/maven-wrapper.jar`: The Maven Wrapper binary.
- `.mvn/wrapper/maven-wrapper.properties`: Configuration file for the Maven Wrapper.

### Using Maven Wrapper

Instead of running Maven commands directly (e.g., `mvn clean install`), you should use the Maven Wrapper:

- On Unix-based systems: `./mvnw clean install`
- On Windows systems: `mvnw.cmd clean install`

By using the wrapper, you ensure that the correct Maven version specified in the wrapper configuration is used.

## 2. Spotless - Maven Plugin

Spotless is a code formatting plugin for Maven that helps enforce consistent code style across a project. By automatically formatting code according to defined rules, Spotless helps maintain clean, readable, and standardized code. This is crucial in collaborative environments where multiple developers contribute to the same codebase.

### Why Spotless is Important for Clean Code

1. **Consistency**: Spotless ensures that all code adheres to a consistent style, which makes the codebase easier to read and maintain. Consistency in formatting reduces cognitive load when navigating and understanding code.

2. **Readability**: Well-formatted code is more readable, which is a cornerstone of clean code principles. Spotless automatically enforces formatting rules, reducing the chances of poorly formatted code being committed.

3. **Focus on Logic**: By automating the formatting process, developers can focus more on writing clean, logical code rather than worrying about style issues.

4. **Reduce Code Review Overhead**: Consistent formatting means code reviews can focus on the logic and functionality of the code rather than style inconsistencies.

5. **Automation**: Spotless integrates seamlessly into the build process, ensuring that code is formatted correctly every time it is built or before it is committed.

**[⬆ back to top](#table-of-contents)**

### Spotless Maven Plugin Configuration

Below is an example configuration of the Spotless Maven plugin in a `pom.xml` file:

```xml
<!-- Spotless plugin -->
<plugin>
    <groupId>com.diffplug.spotless</groupId>
    <artifactId>spotless-maven-plugin</artifactId>
    <version>2.43.0</version>
    <configuration>
        <java>
            <!-- Define which Java files should be included in the formatting process -->
            <includes>
                <include>src/main/java/**/*.java</include>
                <include>src/test/java/**/*.java</include>
            </includes>

            <!-- Google Java Format -->
            <googleJavaFormat>
                <version>${google.format.version}</version>
                <reflowLongStrings>true</reflowLongStrings>
                <formatJavadoc>false</formatJavadoc>
            </googleJavaFormat>

            <!-- Remove Unused Imports: Automatically removes import statements that are not used in the code. -->
            <removeUnusedImports />

            <!-- Trim Trailing Whitespace: Removes any trailing whitespace from each line. -->
            <trimTrailingWhitespace />

            <!-- Indent: Defines the indentation settings for the codebase. -->
            <indent>
                <spaces>true</spaces> <!-- Uses spaces for indentation not tabs -->
                <spacesPerTab>2</spacesPerTab> <!-- define each tab equals 2 spaces -->
            </indent>

        </java>
    </configuration>
</plugin>
```

#### Configuration Details

- **Includes**: Specifies which files should be processed by Spotless. In this example, all Java files in the `src/main/java` and `src/test/java` directories are included.
- **Google Java Format**: Applies the Google Java formatting style. The configuration allows customization, such as reflowing long strings and excluding Javadoc formatting.

  - `reflowLongStrings`: When set to `true`, long strings are automatically wrapped according to the line length.
  - `formatJavadoc`: If `false`, Javadoc comments are not formatted by Spotless.

- **Remove Unused Imports**: This option automatically removes any import statements that are not used in the code. This keeps the code clean and free of unnecessary dependencies.

- **Trim Trailing Whitespace**: Removes any trailing whitespace from each line, which helps in maintaining a clean and professional-looking codebase.

- **Indent**: Defines the indentation settings. In this example, spaces are used instead of tabs, and each tab is equivalent to 2 spaces.

### How to Use Spotless

1. **Setup**: Add the Spotless plugin configuration to your `pom.xml` file as shown above.

2. **Run Spotless**: You can run Spotless by executing the following Maven-Wrapper command:

   ```bash
   ./mvnw spotless:apply
   ```

   This will automatically format your code according to the rules defined in the configuration.

3. **Integrate with CI/CD**: Spotless can be integrated into your CI/CD pipeline to ensure that code formatting is always enforced before code is merged. With that we would introduce pre-commit hocks, enabling code check prior to merging.

**[⬆ back to top](#table-of-contents)**

## 3. Checkstyle

Checkstyle is a static code analysis tool used in Java projects to ensure that your code adheres to a specified coding standard. By integrating Checkstyle into your Maven build process, you can automatically enforce consistent code formatting and detect potential code quality issues early in the development cycle. This helps in maintaining clean, readable, and maintainable code across the entire project.

### Why Use Checkstyle?

- **Enforce Coding Standards**: Checkstyle helps enforce a consistent coding style, making the codebase more uniform and easier to read. For Simpl it shall be configured to follow the Google Java Style Guide. Details to be documented and added to the dev-handbook.

- **Early Detection of Issues**: By integrating Checkstyle into your build process, issues related to coding standards, such as naming conventions, line length, and indentation, are detected early, allowing developers to address them before they become problematic.

- **Automated Code Review**: Checkstyle acts as an automated code reviewer, highlighting potential issues before code is committed or merged, thus improving code quality without requiring manual review of style issues.

- **Customizable Rules**: Checkstyle allows you to customize the ruleset to fit your project's specific needs. You can tailor it to enforce only the rules that are relevant to your team or project.

### Configuring Checkstyle with Maven

To integrate Checkstyle with Maven, you can use the Maven Checkstyle Plugin. Below is an example configuration that you can add to your `pom.xml` file:

```xml
<!-- CheckStyle Plugin -->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-checkstyle-plugin</artifactId>
    <version>3.4.0</version>
    <executions>
        <execution>
            <phase>validate</phase>
            <goals>
                <goal>check</goal>
            </goals>
        </execution>
    </executions>

    <dependencies>
        <dependency>
            <groupId>com.puppycrawl.tools</groupId>
            <artifactId>checkstyle</artifactId>
            <version>10.17.0</version>
        </dependency>
    </dependencies>

    <configuration>
        <configLocation>checkstyle.xml</configLocation>
        <consoleOutput>true</consoleOutput>
        <failsOnError>true</failsOnError>
    </configuration>
</plugin>
```

### Configuration Details

- **Executions**: The Checkstyle plugin is configured to run during the `validate` phase of the Maven build lifecycle. This ensures that the code is checked for style violations early in the build process.

- **Dependencies**: The plugin depends on the Checkstyle library, which is included as a dependency. The specified version is `10.17.0`.

- **Configuration**:
  - `configLocation`: Specifies the location of the Checkstyle configuration file (`checkstyle.xml`). This file contains the rules that Checkstyle will enforce. Each of our repos shall have this as a standard in the root of the repo.
  - `consoleOutput`: When set to `true`, Checkstyle outputs the results of the analysis to the console, making it easy to see any issues during the build.
  - `failsOnError`: If set to `true`, the build will fail if Checkstyle detects any violations. This ensures that code with style issues is not allowed to proceed through the build process. Important on central pipeline steps e.g. building a release where this has to be `true`

### Running Checkstyle

To run Checkstyle as part of your Maven build, you can use the following command:

```bash
./mvnw jxr:jxr jxr:test-jxr checkstyle:checkstyle
```

This command will generate the Checkstyle report, along with JXR (Java Cross Reference) reports, which help in navigating through the source code.

### Benefits of Integrating Checkstyle

- **Improved Code Quality**: Consistently applying coding standards leads to a more readable and maintainable codebase, reducing the likelihood of bugs and improving overall software quality.

- **Automated Enforcement**: By automating the enforcement of coding standards, Checkstyle reduces the need for manual code style reviews, freeing up time for more critical aspects of code review.

- **Early Feedback**: Developers receive immediate feedback on code style violations, enabling them to correct issues before they become ingrained in the codebase.

**[⬆ back to top](#table-of-contents)**

## 4. Allure reports

...

- add allure server

**[⬆ back to top](#table-of-contents)**

## 5. Using REST assured

...  
**[⬆ back to top](#table-of-contents)**

## 6. Using Playwright

...  
**[⬆ back to top](#table-of-contents)**

## 7. Code Documentation and Java Doc

...  
**[⬆ back to top](#table-of-contents)**

## 8. vscode set-up

...  
**[⬆ back to top](#table-of-contents)**

## 9. Intellij set-up

...  
**[⬆ back to top](#table-of-contents)**

## 10. Derived Guiding principles - Simpl Clean Code

...  
**[⬆ back to top](#table-of-contents)**

## 11. Playing with the sandbox

...  
**[⬆ back to top](#table-of-contents)**

Just pull the repo and install a PostgreSQL db at your machine and start the service. In the root of the sandbox create `.env` file and provide:

```env
# test DB
DB_URL=jdbc:postgresql://localhost:5432/your-db-name
DB_USERNAME=USER
DB_PASSWORD="PASSWORD"
DB_DRIVER=org.postgresql.Driver

# Jwt master key
JWT_SECRET="YOUR_KEY_GENERATE_ONE"

```

from a terminal just run `./test_runner.sh`... mac/linux only. For Windows ... pls add something. The allure server will start for you. To access the OpenAPI interface ... just follow the standard Swagger URL.

**[⬆ back to top](#table-of-contents)**

## 12. Test case storage for "int" and "unit"

### 12. Test Case Storage for "int" and "unit"

It is crucial for "Simpl", maintaining a well-organized test structure for both unit and integration tests allowing easy accessibility, automation and management. In the `sandbox-clean-code` example, unit tests (`unit`) and integration tests (`int`) are stored in separate directories under `test/java/com/brooklyn/api/controllers` path. This organization offers several benefits and aligns with best practices for project structure.

#### Why It Is Recommended to Keep Unit and Integration Tests with the Project

1. **Ease of Access and Maintenance**: Storing unit and integration tests within the project directory ensures that they are always in sync with the source code. Developers can easily locate the corresponding tests for any part of the codebase, which simplifies the process of updating tests when the code changes.

2. **Project Cohesion**: Keeping tests alongside the source code promotes project cohesion. It makes it clear that tests are an integral part of the development process, not an afterthought. This encourages a test-driven development (TDD) approach where tests are developed alongside the code.

3. **Consistent Build Process**: When tests are stored within the project, they can be automatically included in the build and CI/CD processes. This ensures that tests are consistently run whenever the project is built, reducing the risk of deploying code that hasn't been properly tested.

4. **Separation of Concerns**: By organizing tests into `unit` and `int` directories, developers can clearly distinguish between unit tests, which validate the functionality of individual components in isolation, and integration tests, which verify the correct interaction between components. This separation helps in managing test scope and focus.

5. **Improved Collaboration**: A well-structured test directory enhances collaboration among team members. New developers can quickly understand the testing strategy and locate specific tests without sifting through a disorganized file structure.

#### Guiding Principle for Project "Simpl"

**Simpl Guiding Principle # XXX: Unit- and Int-Test case organization**:
_Tests are first-class citizens of the "Simpl" codebase. Unit and integration tests must be stored within the project directory structure, organized by type and proximity to the corresponding source code. This approach ensures that tests are always up-to-date, easily accessible, are automatizable, and integral to the development lifecycle, fostering a culture of continuous testing (TDD) and quality assurance._

This principle reinforces the idea that testing is not optional but a critical component of the software development process for Simpl. By adhering to this principle, the "Simpl" project will maintain a robust and reliable codebase, with tests that are easy to manage, run, and update as the project evolves.

**[⬆ back to top](#table-of-contents)**

## 13. Test runner local pipeline

- test reports  


**[⬆ back to top](#table-of-contents)**

## 14. SonarQube

...  
**[⬆ back to top](#table-of-contents)**
